package dam.androidjavierjuanuceda.u4t7preferences;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity
        implements RadioGroup.OnCheckedChangeListener, AdapterView.OnItemSelectedListener, View.OnClickListener {

    private final String MYPREFS = "MyPrefs";
    private EditText etPlayerName;
    private Spinner spinnerLevel, spinnerColor;
    private EditText etScore;
    private Button btQuit;
    private CheckBox soundActive;
    private RadioGroup difficulty;
    private int option_selected, color;
    private ConstraintLayout constraintLayout;
    private ArrayAdapter<CharSequence> spinnerAdapterColorValues;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUI();
    }

    private void setUI() {
        constraintLayout = findViewById(R.id.pantalla);
        etPlayerName = findViewById(R.id.etPlayerName);
        spinnerLevel = findViewById(R.id.spinnerLevel);
        spinnerColor = findViewById(R.id.spinnerColor);
        etScore = findViewById(R.id.etScore);
        btQuit = findViewById(R.id.btQuit);
        soundActive = findViewById(R.id.soundActive);
        difficulty = findViewById(R.id.difficulty);


        ArrayAdapter<CharSequence> spinnerAdapterLevel = ArrayAdapter.createFromResource(
                this, R.array.levels, android.R.layout.simple_spinner_item);
        spinnerAdapterLevel.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        ArrayAdapter<CharSequence> spinnerAdapterColor = ArrayAdapter.createFromResource(
                this, R.array.colors, android.R.layout.simple_spinner_item);
        spinnerAdapterColor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerAdapterColorValues = ArrayAdapter.createFromResource(
                this, R.array.colorsvalues, android.R.layout.simple_spinner_item);
        spinnerAdapterColor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        spinnerLevel.setAdapter(spinnerAdapterLevel);
        spinnerColor.setAdapter(spinnerAdapterColor);

        spinnerColor.setOnItemSelectedListener(this);
        btQuit.setOnClickListener(this);
        difficulty.setOnCheckedChangeListener(this);
        option_selected = 0;
        spinnerColor.setSelection(4);
    }


    @Override
    protected void onPause() {
        super.onPause();

        SharedPreferences myPreferences = getSharedPreferences(MYPREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = myPreferences.edit();

        editor.putString("PlayerName", etPlayerName.getText().toString());
        editor.putInt("Level", spinnerLevel.getSelectedItemPosition());
        editor.putInt("Score", Integer.parseInt(etScore.getText().toString()));

        editor.putBoolean("SoundActive", soundActive.isChecked());
        editor.putInt("Color", color);
        editor.putInt("Difficulty", option_selected);

        editor.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences myPreferences = getSharedPreferences(MYPREFS, MODE_PRIVATE);

        etPlayerName.setText(myPreferences.getString("PlayerName", "unknown"));
        spinnerLevel.setSelection(myPreferences.getInt("Level", 0));
        etScore.setText(String.valueOf(myPreferences.getInt("Score", 0)));

        soundActive.setChecked(myPreferences.getBoolean("SoundActive", false));
        spinnerColor.setSelection(myPreferences.getInt("Color", 4));
        constraintLayout.setBackgroundColor(myPreferences.getInt("Color", 4));
        difficulty.check(myPreferences.getInt("Difficulty", 0));

    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        option_selected = checkedId;
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

       try {
           color = position;
           constraintLayout.setBackgroundColor(Color.parseColor((String) spinnerAdapterColorValues.getItem(position)));
       }
       catch (Exception e) {
           constraintLayout.setBackgroundColor(Color.parseColor("#FFFFFF"));
       }


    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        constraintLayout.setBackgroundColor(getResources().getColor(R.color.colorwhite));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btQuit: finish(); break;
            default: break;
        }
    }
}
