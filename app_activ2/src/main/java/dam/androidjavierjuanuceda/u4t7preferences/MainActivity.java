package dam.androidjavierjuanuceda.u4t7preferences;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity
        implements RadioGroup.OnCheckedChangeListener, AdapterView.OnItemSelectedListener, View.OnClickListener {

    protected static final String MYPREFS = "MyPrefs";
    private EditText etPlayerName;
    private Spinner spinnerLevel, spinnerColor;
    private EditText etScore;
    private Button btQuit, bt_show_preferences_file;
    private CheckBox soundActive;
    private RadioGroup difficulty, preferences;
    private int option_selected, color;
    private ConstraintLayout constraintLayout;
    private String[] spinnerAdapterColorValues;
    private static MainActivity myContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUIfindViewById();
        setUIArrayAdapter();
        setUIListener();
    }

    private void setUIfindViewById() {
        constraintLayout = findViewById(R.id.pantalla);
        etPlayerName = findViewById(R.id.etPlayerName);
        spinnerLevel = findViewById(R.id.spinnerLevel);
        spinnerColor = findViewById(R.id.spinnerColor);
        etScore = findViewById(R.id.etScore);
        preferences = findViewById(R.id.rgpreferences);
        btQuit = findViewById(R.id.btQuit);
        soundActive = findViewById(R.id.soundActive);
        difficulty = findViewById(R.id.difficulty);
        bt_show_preferences_file = findViewById(R.id.bt_show_preferences_file);
    }

    private void setUIArrayAdapter() {
        ArrayAdapter<CharSequence> spinnerAdapterLevel = ArrayAdapter.createFromResource(this, R.array.levels, android.R.layout.simple_spinner_item);
        spinnerAdapterLevel.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLevel.setAdapter(spinnerAdapterLevel);

        ArrayAdapter<CharSequence> spinnerAdapterColor = ArrayAdapter.createFromResource(this, R.array.colors, android.R.layout.simple_spinner_item);
        spinnerAdapterColor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerAdapterColorValues = getResources().getStringArray(R.array.colorsvalues);
        spinnerColor.setAdapter(spinnerAdapterColor);
        spinnerColor.setOnItemSelectedListener(this);

    }

    private void setUIListener() {
        btQuit.setOnClickListener(this);
        bt_show_preferences_file.setOnClickListener(this);
        option_selected = 0;
        difficulty.setOnCheckedChangeListener(this);
        preferences.setOnCheckedChangeListener(this);
        read_write(true);
        myContext = this;
    }

    public static MainActivity getContext() {
        return myContext;
    }

    public RadioGroup getPreferences() {
        return preferences;
    }

    @Override
    protected void onPause() {
        super.onPause();
        read_write(false);
    }

    private void read_write(boolean read) {
        SharedPreferences myPreferences = put_content_file_preferences(preferences.getCheckedRadioButtonId());
        if (read) {
            etPlayerName.setText(myPreferences.getString("PlayerName", getString(R.string.unknown)));
            spinnerLevel.setSelection(myPreferences.getInt("Level", 0));
            etScore.setText(String.valueOf(myPreferences.getInt("Score", 0)));
            soundActive.setChecked(myPreferences.getBoolean("SoundActive", false));
            spinnerColor.setSelection(myPreferences.getInt("Color", 4));
            difficulty.check(myPreferences.getInt("Difficulty", 0));
            preferences.check(myPreferences.getInt("Preferences", R.id.rbsharedpreferences));
        } else {
            SharedPreferences.Editor editor = myPreferences.edit();
            editor.putString("PlayerName", etPlayerName.getText().toString());
            editor.putInt("Level", spinnerLevel.getSelectedItemPosition());
            editor.putInt("Score", Integer.parseInt(etScore.getText().toString()));
            editor.putBoolean("SoundActive", soundActive.isChecked());
            editor.putInt("Color", color);
            editor.putInt("Difficulty", option_selected);
            editor.commit();
        }
    }

    @SuppressLint("NewApi")
    private SharedPreferences put_content_file_preferences(int filePreferences) {
        SharedPreferences sharedPreferences;

        switch (filePreferences) {
            // En getPreferences tiene el nombre del paquete de la aplicacion
            // Representa las preferencias de varias activity del paquete android.

            case R.id.rbpreferences: sharedPreferences = getPreferences(MODE_PRIVATE); break;

            // En PreferenceManager.getDefaultSharedPreferences tiene el nombre
            // de la clase
            // Representa las preferencias de la clase android.

            case R.id.rbdefaultsharedpreferences: sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);break;

            // En getSharedPreferences tiene el nombre que se le ponga en el primer pàrametro
            // Representa las preferencias de varias activity android.

            case R.id.rbsharedpreferences:
            default: sharedPreferences = getSharedPreferences(MYPREFS, MODE_PRIVATE); break;
        }

        return sharedPreferences;

    }

    @SuppressLint("NewApi")
    private void put_preference() {
        put_preference_in_files(R.id.rbpreferences);
        put_preference_in_files(R.id.rbsharedpreferences);
        put_preference_in_files(R.id.rbdefaultsharedpreferences);
    }

    private void put_preference_in_files(int file) {
        SharedPreferences.Editor editor = put_content_file_preferences(file).edit();
        editor.putInt("Preferences", preferences.getCheckedRadioButtonId());
        editor.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        read_write(true);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

        switch (group.getId()) {
            case R.id.difficulty: option_selected = checkedId; break;
            case R.id.rgpreferences: put_preference(); break;
            default: break;
        }
    }


    @SuppressLint("ResourceAsColor")
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        try {
            color = position;
            constraintLayout.setBackgroundColor(Color.parseColor(spinnerAdapterColorValues[position]));
        } catch (Exception e) {
            constraintLayout.setBackgroundColor(R.color.colorwhite);
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        constraintLayout.setBackgroundColor(getResources().getColor(R.color.colorwhite));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btQuit: finish(); break;
            case R.id.bt_show_preferences_file:
                read_write(false);
                startActivity(new Intent(this, NewActivity.class));
                break;
            default: break;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("file_preferences", preferences.getCheckedRadioButtonId());
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        read_write(true);
        preferences.check(savedInstanceState.getInt("file_preferences", R.id.rbsharedpreferences));
    }
}
