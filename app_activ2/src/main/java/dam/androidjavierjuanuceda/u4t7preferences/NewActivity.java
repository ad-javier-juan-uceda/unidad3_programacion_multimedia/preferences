package dam.androidjavierjuanuceda.u4t7preferences;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.TextView;

public class NewActivity extends AppCompatActivity {
    private TextView content_preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new);
        setUI();
    }

    private void setUI() {
        content_preferences = findViewById(R.id.content_preferences);
        content_preferences.setText(put_content_file_preferences());
    }

    @SuppressLint("NewApi")
    private String put_content_file_preferences() {
        StringBuilder content_file = new StringBuilder();
        SharedPreferences sharedPreferences;
        MainActivity mainActivity = MainActivity.getContext();
        switch (mainActivity.getPreferences().getCheckedRadioButtonId()) {
            case R.id.rbpreferences:
                sharedPreferences = mainActivity.getPreferences(MODE_PRIVATE);
                break;
            case R.id.rbdefaultsharedpreferences:
                sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
                break;
            case R.id.rbsharedpreferences:
            default:
                sharedPreferences = getSharedPreferences(MainActivity.MYPREFS, MODE_PRIVATE);
                break;
        }
        content_file.append(getString(R.string.player) + " " + sharedPreferences.getString("PlayerName", getString(R.string.unknown)) + System.lineSeparator());
        content_file.append(getString(R.string.score) + " " + sharedPreferences.getInt("Score", 0) + System.lineSeparator());
        content_file.append(getString(R.string.level) + " " + sharedPreferences.getInt("Level", 0) + System.lineSeparator());
        content_file.append(getString(R.string.difficulty) + " " + sharedPreferences.getInt("Difficulty", 0) + System.lineSeparator());
        content_file.append(getString(R.string.sound) + " " + sharedPreferences.getBoolean("SoundActive", false) + System.lineSeparator());
        content_file.append(getString(R.string.bg_color) + " " + sharedPreferences.getInt("Color", 4) + System.lineSeparator());

        return content_file.toString();
    }
}
